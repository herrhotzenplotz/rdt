/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <rdt/mainwindow.hh>

#include <common/network.hh>

#include <QMenuBar>

namespace rdt {

    MainWindow::MainWindow()
        : m_drawingwidget(new DrawingWidget())
        , m_udpsocket(nullptr)
    {
        setWindowTitle(tr("rdt master"));
        setCentralWidget(this->m_drawingwidget);

        this->createActions();

        this->m_udpsocket = new QUdpSocket(this);
        this->m_udpsocket->bind(QHostAddress::AnyIPv4);

        QObject::connect(this->m_drawingwidget, &DrawingWidget::rdtEvent,
                         this, &MainWindow::networkEvent);
    }

    void MainWindow::createActions(void)
    {
        QMenu *file_menu = this->menuBar()->addMenu(tr("&File"));
        file_menu->addAction(QIcon::fromTheme("application-exit"),
                             tr("E&xit"),
                             this,
                             &QWidget::close);

        QMenu *edit_menu = this->menuBar()->addMenu(tr("&Edit"));
        edit_menu->addAction(QIcon::fromTheme("document-new"),
                             tr("&Clear whiteboard"),
                             this->m_drawingwidget,
                             &DrawingWidget::onClearEvent);

    }

    void MainWindow::networkEvent(RDTMessage message)
    {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream.setVersion(QDataStream::Qt_4_8);

        stream << message;

        this->m_udpsocket->writeDatagram(data, QHostAddress::Broadcast, RDT_MULTICAST_PORT);
    }

    MainWindow::~MainWindow()
    {
        if (this->m_udpsocket)
            delete this->m_udpsocket;
    }

}
