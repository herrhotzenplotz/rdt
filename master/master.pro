QT          +=  core widgets network
requires(qtConfig(udpsocket))

TARGET       = rdt-master

SOURCES     +=	src/rdt.cc \
                src/mainwindow.cc

HEADERS     +=  include/rdt/mainwindow.hh

INCLUDEPATH +=  include \
                ../common/include/

LIBS        += -L../common -lcommon
