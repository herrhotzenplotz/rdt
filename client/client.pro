QT          +=  core network widgets

TARGET  =       rdt-client

SOURCES =       src/main.cc \
                src/mainwindow.cc
HEADERS =       include/rdt/mainwindow.hh

INCLUDEPATH +=  include ../common/include

LIBS += -L../common/ -lcommon
