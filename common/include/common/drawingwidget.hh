/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DRAWINGWIDGET_H
#define DRAWINGWIDGET_H

#include <QWidget>
#include <QColor>
#include <QColor>
#include <QPen>
#include <QTabletEvent>

#include <common/message.hh>

namespace rdt
{
    class DrawingWidget : public QWidget
    {
        Q_OBJECT

        enum Valuator { PressureValuator, TangentialPressureValuator,
            TiltValuator, VTiltValuator, HTiltValuator, NoValuator };

    public:
        DrawingWidget();

    protected:
        void tabletEvent(QTabletEvent *event) override;
        void paintEvent(QPaintEvent *event) override;

    signals:
        void rdtEvent(RDTMessage message);

    public slots:
        void onRdtEvent(RDTMessage& message);
        void onClearEvent(void);

    private:
        void initPixmap(void);
        void updateBrush(const QTabletEvent *event);
        void paintPixmap(QPainter &painter, QTabletEvent *event);
        static qreal pressureToWidth(qreal pressure);

        QPixmap m_pixmap;
        QColor  m_color;
        QPen    m_pen;
        QBrush  m_brush;
        bool    m_devicedown = false;

        Valuator m_alphaChannelValuator = TangentialPressureValuator;
        Valuator m_colorSaturationValuator = NoValuator;
        Valuator m_lineWidthValuator = PressureValuator;

        struct Point {
            QPointF pos;
            qreal pressure = 0;
            qreal rotation = 0;
        } m_lastpoint;
    };
}

#endif /* DRAWINGWIDGET_H */
