QT      +=  core widgets
TEMPLATE = lib
CONFIG  += staticlib

SOURCES = src/message.cc \
          src/drawingwidget.cc
HEADERS = include/common/message.hh \
          include/common/network.hh \
          include/common/drawingwidget.hh

TARGET  = common

INCLUDEPATH +=  include
