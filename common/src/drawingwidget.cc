/*
 * Copyright 2021 Nico Sonack <nsonack@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Heavily based on
 * https://code.qt.io/cgit/qt/qtbase.git/tree/examples/widgets/widgets/tablet/tabletapplication.cpp
 */

/*
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of The Qt Company Ltd nor the names of its
 **     contributors may be used to endorse or promote products derived
 **     from this software without specific prior written permission.
 **
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
*/

#include <common/drawingwidget.hh>

#include <QDebug>
#include <QPainter>

namespace rdt
{

    DrawingWidget::DrawingWidget()
        : QWidget(nullptr)
    {
        setAutoFillBackground(true);
        setAttribute(Qt::WA_TabletTracking);
        setMinimumSize({ 1200, 800});
    }


    void DrawingWidget::tabletEvent(QTabletEvent *event)
    {
        auto brush = event->pointerType() == QTabletEvent::Eraser
            ? RDTMessage::BrushType::Eraser
            : RDTMessage::BrushType::Pen;

        switch (event->type())
        {
        case QEvent::TabletMove: {
            if (m_devicedown) {
                updateBrush(event);

                QPainter painter(&m_pixmap);

                paintPixmap(painter, event);

                m_lastpoint.pos = event->posF();
                m_lastpoint.pressure = event->pressure();
                m_lastpoint.rotation = event->rotation();

                RDTMessage m { RDTMessage::Type::move, brush, event->posF(), event->pressure() };
                emit rdtEvent(m);
            }
        } break;
        case QEvent::TabletPress: {
            if (!this->m_devicedown) {
                this->m_devicedown = true;

                m_lastpoint.pos = event->posF();
                m_lastpoint.pressure = event->pressure();
                m_lastpoint.rotation = event->rotation();

                RDTMessage m { RDTMessage::Type::down, brush, event->posF(), event->pressure() };
                emit rdtEvent(m);
            }
        } break;
        case QEvent::TabletRelease: {
            this->m_devicedown = false;
            update();

            RDTMessage m { RDTMessage::Type::release, brush, event->posF(), event->pressure() };
            emit rdtEvent(m);
        } break;
        default:
            break;
        }
    }

    void DrawingWidget::initPixmap(void)
    {
        qreal dpr = devicePixelRatioF();
        QPixmap newPixmap = QPixmap(qRound(width() * dpr), qRound(height() * dpr));

        newPixmap.setDevicePixelRatio(dpr);
        newPixmap.fill(Qt::white);

        QPainter painter(&newPixmap);
        if (!m_pixmap.isNull())
            painter.drawPixmap(0, 0, m_pixmap);
        painter.end();
        m_pixmap = newPixmap;
    }

    void DrawingWidget::paintEvent(QPaintEvent *event)
    {
        if (m_pixmap.isNull())
            initPixmap();

        QPainter painter(this);
        QRect pixmapPortion = QRect(event->rect().topLeft() * devicePixelRatioF(),
                                    event->rect().size() * devicePixelRatioF());
        painter.drawPixmap(event->rect().topLeft(), m_pixmap, pixmapPortion);
    }

    void DrawingWidget::updateBrush(const QTabletEvent *event)
    {
        int hue, saturation, value, alpha;
        m_color.getHsv(&hue, &saturation, &value, &alpha);

        int vValue = int(((event->yTilt() + 60.0) / 120.0) * 255);
        int hValue = int(((event->xTilt() + 60.0) / 120.0) * 255);


        switch (m_alphaChannelValuator) {
        case PressureValuator:
            m_color.setAlphaF(event->pressure());
            break;
        case TangentialPressureValuator:
            if (event->deviceType() == QTabletEvent::Airbrush)
                m_color.setAlphaF(qMax(0.01, (event->tangentialPressure() + 1.0) / 2.0));
            else
                m_color.setAlpha(255);
            break;
        case TiltValuator:
            m_color.setAlpha(std::max(std::abs(vValue - 127),
                                      std::abs(hValue - 127)));
            break;
        default:
            m_color.setAlpha(255);
        }

        switch (m_colorSaturationValuator) {
        case VTiltValuator:
            m_color.setHsv(hue, vValue, value, alpha);
            break;
        case HTiltValuator:
            m_color.setHsv(hue, hValue, value, alpha);
            break;
        case PressureValuator:
            m_color.setHsv(hue, int(event->pressure() * 255.0), value, alpha);
            break;
        default:
            break;
        }

        switch (m_lineWidthValuator) {
        case PressureValuator:
            m_pen.setWidthF(pressureToWidth(event->pressure()));
            break;
        case TiltValuator:
            m_pen.setWidthF(std::max(std::abs(vValue - 127),
                                     std::abs(hValue - 127)) / 12);
            break;
        default:
            m_pen.setWidthF(1);
        }

        if (event->pointerType() == QTabletEvent::Eraser) {
            m_brush.setColor(Qt::white);
            m_pen.setColor(Qt::white);
            m_pen.setWidthF(event->pressure() * 30 + 1);
        } else {
            m_brush.setColor(m_color);
            m_pen.setColor(m_color);
        }
    }

    void DrawingWidget::paintPixmap(QPainter &painter, QTabletEvent *event)
    {
        static qreal maxPenRadius = pressureToWidth(1.0);
        painter.setRenderHint(QPainter::Antialiasing);

        switch (event->deviceType()) {
        case QTabletEvent::Stylus: {
            painter.setPen(m_pen);
            painter.drawLine(m_lastpoint.pos, event->posF());
            update(QRect(m_lastpoint.pos.toPoint(), event->pos()).normalized()
                   .adjusted(-maxPenRadius, -maxPenRadius, maxPenRadius, maxPenRadius));
        } break;
        default: {
            // Ignored
        } break;
        }
    }

    qreal DrawingWidget::pressureToWidth(qreal pressure)
    {
        return pressure * 6 + 1;
    }

    void DrawingWidget::onRdtEvent(RDTMessage& msg)
    {
        QPainter painter(&m_pixmap);
        static qreal maxPenRadius = pressureToWidth(1.0);
        painter.setRenderHint(QPainter::Antialiasing);

        switch (msg.type) {
        case RDTMessage::Type::move:
        case RDTMessage::Type::down: {
            if (!this->m_devicedown) {
                this->m_devicedown = true;
                m_lastpoint.pos = msg.pos;
                m_lastpoint.pressure = msg.pressure;
            }

        } break;
        case RDTMessage::Type::release: {
            this->m_devicedown = false;
            update();

            return;
        } break;
        case RDTMessage::Type::clrscrn: {
            qDebug() << "Clear event received";
            this->m_pixmap.fill();
            update();

            return;
        }
        }

        if (this->m_devicedown) {
            if (msg.brushtype == RDTMessage::BrushType::Eraser) {
                m_brush.setColor(Qt::white);
                m_pen.setColor(Qt::white);
                m_pen.setWidthF(msg.pressure * 30 + 1);
            } else {
                m_brush.setColor(m_color);
                m_pen.setColor(m_color);
                m_pen.setWidthF(pressureToWidth(msg.pressure));
            }

            painter.setPen(m_pen);
            painter.drawLine(m_lastpoint.pos, msg.pos);
            update(QRect(m_lastpoint.pos.toPoint(), msg.pos.toPoint()).normalized()
                   .adjusted(-maxPenRadius, -maxPenRadius, maxPenRadius, maxPenRadius));
        }

        m_lastpoint.pos = msg.pos;
        m_lastpoint.pressure = msg.pressure;

    }

    void DrawingWidget::onClearEvent(void)
    {
        this->m_pixmap.fill();
        qDebug() << "Sending clear event";
        RDTMessage m { RDTMessage::Type::clrscrn, RDTMessage::BrushType::Eraser, {}, {} };
        emit rdtEvent(m);

        update();
    }

}
